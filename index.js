module.exports = {
  bracketSpacing: true,
  singleQuote: true,
  printWidth: 120,
  semi: false,
  arrowParens: 'always',
  trailingComma: 'all'
}